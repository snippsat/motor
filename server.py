from flask import Flask, render_template, jsonify

app = Flask(__name__)
@app.route('/')
def view():
    return render_template("index.html")

@app.route('/motor_start')
def motor_start():
    '''Do motor start call to PLS(RasPi)'''
    # http://adresse.lan/start_pumpe
    d = {}
    d['color'] = 'Green'
    d['status'] = 'Run'
    return jsonify(d)

@app.route('/motor_stop')
def motor_stop():
    '''Do motor stop call to PLS(RasPi)'''
    # http://adresse.lan/stop_pumpe
    d = {}
    d['color'] = 'Red'
    d['status'] = 'Stop'
    return jsonify(d)

@app.route('/motor_lamp_status')
def motor_status():
    '''Getting stauts from PLS(RasPi)'''
    # http://adresse.lan/get_status
    pass

if __name__ == '__main__':
    app.run(debug=True)
