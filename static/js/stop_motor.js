$(document).ready(function() {
  $("#stop").click(function() {
    $.getJSON('/motor_stop', function(dat) {
      $(".container").css({"background": dat['color']});
      $("#result").text(dat['status']);
    });
  });
});
