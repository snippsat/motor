$(document).ready(function() {
  $("#start").click(function() {
    $.getJSON('/motor_start', function(dat) {
      $(".container").css({"background": dat['color']});
      $("#result").text(dat['status']);
    });
  });
});
